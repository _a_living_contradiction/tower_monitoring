export class ResponseModel {
    status: boolean
    code: number
    statusMessage: string
    data: any
    authToken : any
}