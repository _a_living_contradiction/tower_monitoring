export class Constants{
    
    static LOGIN_RESPONSE : string = "loginResponse"; 
    static LOGGED_USER : string = "loggedUser"; 
    static CURRENT_STATE : string = "currentState"; 
    static PREVIOUS_STATE : string = "previousState"; 

}

export enum HttpVerbs{
    GET = 1,
    POST,
    PUT,
    DELETE
}