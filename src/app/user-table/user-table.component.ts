import { Component, OnInit,ViewChild} from '@angular/core';
import {UserModel} from '../model/user.model';
import {UserFormComponent} from '../user-form/user-form.component';
import {RegisterUserService} from'../services/register-user.service';
import { GridModule,Selection,GridComponent,RowSelectEventArgs} from '@syncfusion/ej2-ng-grids';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {
  saveUser:UserModel={} as UserModel;
  data:any;
  message:string;
  selectedRow:any;
  columns=[
    {field:'name',headerText:'Name',allowEditing:false},
    {field:'address',headerText:'Address',allowEditing:false},
    {field:'email',headerText:'Email',allowEditing:false},
    {field:'password',headerText:'Password',allowEditing:false}
  ] 
  
  constructor(private RUSvc:RegisterUserService) { }

  @ViewChild('grid')
  public grid:GridComponent;

rowSelected(args: GridComponent) {

  let selectedrecords= this.grid.getSelectedRecords();  
  this.selectedRow=selectedrecords[0]
}

  ngOnInit() {
        this.RUSvc.getUserDetails(1).then(UserList=>{
        this.data.forEach(user => {
          UserList.data.forEach(val => {
            if(user.usertype==val.id)
            user.usertype=val.name
          });
        });
      })
    this.message='No Records To Display'
  }
}
