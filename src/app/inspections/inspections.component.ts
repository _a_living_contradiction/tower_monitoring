import { Component, OnInit } from '@angular/core';
import { GridModel, Grid } from '@syncfusion/ej2-grids';

@Component({
  selector: 'app-inspections',
  templateUrl: './inspections.component.html',
  styleUrls: ['./inspections.component.css']
})
export class InspectionsComponent implements OnInit {
 inspect:any=[
  
  {id:1,primary:'Tower-48',distance:'0.147',address:'unnamed'}
]
columns=[
  {field:'id',headerText:'ID',allowEditing:false},
  {field:'primary',headerText:'Primary Tower',allowEditing:false},
  {field:'distance',headerText:'Distance',allowEditing:false},
  {field:'address',headerText:'Address',allowEditing:false}
] 
//data:any[];
  constructor() { }

  ngOnInit() {
   /* this.data = [
      { OrderID: 10248, CustomerID: 'VINET', EmployeeID: 5 },
      { OrderID: 10249, CustomerID: 'TOMSP', EmployeeID: 6 },
      { OrderID: 10250, CustomerID: 'HANAR', EmployeeID: 4 }];*/
  }
  }

