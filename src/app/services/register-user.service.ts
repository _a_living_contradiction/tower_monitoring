import { Injectable, Output,EventEmitter }     from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable, Subject} from 'rxjs';

import { Jsonp} from '@angular/http';
import{BaseService} from '../services/base.service'
import { environment } from '../../environments/environment';
import { Constants} from '../model/constants.model';
import { ResponseModel } from '../model/response.model';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { UserModel } from 'src/app/model/user.model';
@Injectable()
export class RegisterUserService {
    private baseUrl = environment.BASE_URL;
    constructor(private http: Http,private baseSvc:BaseService) {}
    
    Register(userId:UserModel){
      return this.baseSvc.invokeService(this.baseUrl+'account/'+userId.vendortype.id+'/signup',userId,1,'')
   }

   UpdateUser(UserData:UserModel){
    return this.baseSvc.invokeService(this.baseUrl+UserData.email,UserData,4)
 }
 getUserDetails(userId){
  return this.baseSvc.invokeService(this.baseUrl+'account/users','',2)
}
getUserTypeList(){
  return this.baseSvc.invokeService(this.baseUrl+'MasterData/vendorTypes','',2)
}
  }