import { Injectable, Output,EventEmitter }     from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable, Subject} from 'rxjs';

import { Jsonp} from '@angular/http';

import { environment } from '../../environments/environment';
import { Constants} from './../model/constants.model';
import {ResponseModel}  from './../model/response.model';



// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/topromise';

@Injectable()
export class BaseService {
    
    protected baseUrl:string =  environment.BASE_URL;
    public errorMessage: EventEmitter<string>;
    public loading: EventEmitter<boolean>;
    loadingCount:number=0;
    headers:any;
    // message:any;

    constructor(private http: Http) { 
        this.errorMessage= new EventEmitter();
        this.loading= new EventEmitter();
        
    }

    invokeService(url,data,methodType,h?) : Promise<any> {
        
        this.loading.emit(true);
        this.loadingCount++;
        var resObservable : Observable<any>;
        this.headers = h;
        var httpResponseErr : ResponseModel ;

        switch( methodType ){
            case 1:
            resObservable = this.postData(url,data);
            break;
            case 2:
            resObservable = this.getData(url);
            break;
            case 3:
            resObservable = this.deleteData(url);
            break;
            case 4:
            resObservable = this.putData(url,data);
            break;
        }

        return resObservable
            .map(data => data)
            .toPromise()
            .then(data => {
                this.loadingCount--;
                // if(!data.status){
                //     this.errorMessage.emit(data.statusMessage);
                // }
                if(this.loadingCount!=0)
                    this.loading.emit(true);
                if(this.loadingCount==0)
                    this.loading.emit(false);
                return data;
            })
            // .catch((error: any) => {
            //     httpResponseErr = new ResponseModel();
            //     httpResponseErr.status = false;
            //     httpResponseErr.statusCode = error.status;

            //     if(this.loadingCount!=0)
            //         this.loading.emit(true);
            //     if(this.loadingCount==1){
            //         this.loadingCount--;
            //         this.loading.emit(false);
            //     }

            //     return httpResponseErr;
            // });

            
    } 
    
    
    postData(url, obj): Observable<ResponseModel> {
        return this.http.post(url, obj, { headers: this.tokenAuthorizer() })
            .map(data => {
                var tmp = data.json();
                return tmp;
            })
            .catch((error: any) => Observable.throw(error || 'Server error'));
        
    }

    getData(url): Observable<ResponseModel> {
        return this.http.get(url, { headers: this.tokenAuthorizer() })
            .map(data => { console.log(data); return data.json()})
            .catch((error: any) =>  Observable.throw(error || 'Server error'));
    }

    putData(url, obj): Observable<ResponseModel> {
        return this.http.put(url, obj, { headers: this.tokenAuthorizer() })
            .map(data => data.json())
            .catch((error: any) => Observable.throw(error || 'Server error'));

    }

    deleteData(url): Observable<ResponseModel> {
        return this.http.delete(url, { headers: this.tokenAuthorizer() })
            .map(data => data.json())
            .catch((error: any) => Observable.throw(error || 'Server error'));
    }

    tokenAuthorizer(){
         let headers = new Headers({'Content-Type': 'application/json'}); 
         if(this.headers == '')
            headers = new Headers({});
         
         var currUser = localStorage.getItem(Constants.LOGIN_RESPONSE);
         if(!currUser || currUser == "undefined" || currUser == 'null'){
            localStorage.clear();
            headers.append('Authorization' , "Bearer " +  '');
            return headers;
         }
         else{
            var token = JSON.parse(currUser).token;
            headers.append('Authorization' , "Bearer " +  token);
            return headers;
         }
         
    }


}
