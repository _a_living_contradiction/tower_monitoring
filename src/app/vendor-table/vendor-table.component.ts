import { Component, OnInit } from '@angular/core';
import {vendorsModel} from '../model/vendors.model';
import {VendorFormComponent} from '../vendor-form/vendor-form.component'
@Component({
  selector: 'app-vendor-table',
  templateUrl: './vendor-table.component.html',
  styleUrls: ['./vendor-table.component.css']
})
export class VendorTableComponent implements OnInit {
saveVendors:vendorsModel={} as vendorsModel;
columns=[
  {field:'name',headerText:'Name',allowEditing:false},
  {field:'mail',headerText:'Email',allowEditing:false},
  {field:'address',headerText:'Address',allowEditing:false},
  {field:'password',headerText:'Password',allowEditing:false},
  {field:'vendorType',headerText:'Vendor Type',allowEditing:false}
] 
  constructor() { }

  ngOnInit() {
  }

}
