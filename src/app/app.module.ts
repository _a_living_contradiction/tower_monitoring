import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { UserComponent } from './user/user.component';
import { TowersComponent } from './towers/towers.component';
import { VendorsComponent } from './vendors/vendors.component';
import { InspectionsComponent } from './inspections/inspections.component';
import { MapVendorComponent } from './map-vendor/map-vendor.component';
import { AppRoutingModule } from './/app-routing.module';
import { UserFormComponent } from './user-form/user-form.component';
import { UserTableComponent } from './user-table/user-table.component';
import { TowerFormComponent } from './tower-form/tower-form.component';
import { TowerTableComponent } from './tower-table/tower-table.component';
import { VendorTableComponent } from './vendor-table/vendor-table.component';
import { VendorFormComponent } from './vendor-form/vendor-form.component';
import { GridModule } from'@syncfusion/ej2-ng-grids';

//import { routing } from './app.routing';


import {BaseService} from './services/base.service';
import {RegisterUserService} from './services/register-user.service';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UserComponent,
    TowersComponent,
    VendorsComponent,
    InspectionsComponent,
    MapVendorComponent,
    UserFormComponent,
    UserTableComponent,
    TowerFormComponent,
    TowerTableComponent,
    VendorTableComponent,
    VendorFormComponent,
    LoginComponent,
    
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    //routing,
    //NgPipesModule,
    //JsonpModule
    HttpModule,
    GridModule 
  ],
  providers: [//AppRouteConfig,
    BaseService,
    RegisterUserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
