import { Component, OnInit } from '@angular/core';
import {towerModel} from '../model/towers.model';
@Component({
  selector: 'app-tower-table',
  templateUrl: './tower-table.component.html',
  styleUrls: ['./tower-table.component.css']
})
export class TowerTableComponent implements OnInit {
savetable:towerModel={} as towerModel;
columns=[
  {field:'name',headerText:'Name',allowEditing:false},
  {field:'description',headerText:'Description',allowEditing:false},
  {field:'latitude',headerText:'Latitude',allowEditing:false},
  {field:'longitude',headerText:'Longitude',allowEditing:false},
  {field:'range',headerText:'Range',allowEditing:false}
  
] 
  constructor() { }

  ngOnInit() {
  }

}
