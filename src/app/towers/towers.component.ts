import { Component, OnInit } from '@angular/core';
import {towerModel} from '../model/towers.model';
import {TowerFormComponent} from '../tower-form/tower-form.component';
import {TowerTableComponent} from '../tower-table/tower-table.component'
@Component({
  selector: 'app-towers',
  templateUrl: './towers.component.html',
  styleUrls: ['./towers.component.css']
})
export class TowersComponent implements OnInit {
createTower:boolean=false;
showTable:boolean=true;
  constructor() { }
onCreate(){
  this.createTower=!this.createTower;
  this.showTable=!this.showTable
}
//displayTower(count){

//console.log(count)
//}
valueChanged(evnt){
this.showTable=true;
this.createTower=false;
}
  ngOnInit() {
  }

}
