import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import {vendorsModel} from '../model/vendors.model';
import {FormsModule,NgForm} from '@angular/forms'
@Component({
  selector: 'app-vendor-form',
  templateUrl: './vendor-form.component.html',
  styleUrls: ['./vendor-form.component.css']
})
export class VendorFormComponent implements OnInit {
vendors:vendorsModel={} as vendorsModel;
@Output() vendorChange:EventEmitter<any>=new EventEmitter();
  constructor() { }
onBack(){
  this.vendorChange.emit({name:'sadd'})
}
onCrt(vendorform?:NgForm){
  if(vendorform!=null)
  vendorform.reset();
}
  ngOnInit() {
  }

}
