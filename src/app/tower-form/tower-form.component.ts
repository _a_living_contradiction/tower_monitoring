import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import {towerModel} from '../model/towers.model';
import {FormsModule, NgForm} from '@angular/forms';
import {Router} from '@angular/router'
@Component({
  selector: 'app-tower-form',
  templateUrl: './tower-form.component.html',
  styleUrls: ['./tower-form.component.css']
})
export class TowerFormComponent implements OnInit {
towers:towerModel={} as towerModel;
@Output() valuechange: EventEmitter<any> = new EventEmitter ();

onCrt(towerform?:NgForm){
if(towerform!=null)
towerform.reset()
}
onSubmit(){
  console.log(this.towers);
}
onBack(){
    this.valuechange.emit({name:'sdfsf'})
  // console.log(this.valuechange);

// this._route.navigateByUrl('Towers')
}
  constructor(private _route:Router) { }

  ngOnInit() {
  }

}
