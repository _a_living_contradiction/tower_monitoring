import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TowerFormComponent } from './tower-form.component';

describe('TowerFormComponent', () => {
  let component: TowerFormComponent;
  let fixture: ComponentFixture<TowerFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TowerFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TowerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
