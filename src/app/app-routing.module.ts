import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserComponent} from './user/user.component';
import {TowersComponent} from './towers/towers.component';
import {VendorsComponent} from './vendors/vendors.component';
import {InspectionsComponent} from './inspections/inspections.component';
import {MapVendorComponent} from './map-vendor/map-vendor.component';
import {NavbarComponent} from './navbar/navbar.component';
import {UserFormComponent} from './user-form/user-form.component';
import {UserTableComponent} from './user-table/user-table.component';
import {TowerFormComponent} from './tower-form/tower-form.component';
import {TowerTableComponent} from './tower-table/tower-table.component';
import {LoginComponent} from './login/login.component';

const routes:Routes=[
  {path:' ',component:NavbarComponent},
  {path:'Users',component:UserComponent},
  {path:'UsersForm',component:UserFormComponent},
  {path:'UsersTable',component:UserTableComponent},
  {path:'Towers',component:TowersComponent},
  {path:'TowersForm',component:TowerFormComponent},
  {path:'TowersTable',component:TowerTableComponent},
  {path:'Vendors',component:VendorsComponent},
  {path:'Inspection',component:InspectionsComponent},
  {path:'mapVendor',component:MapVendorComponent},
  {path:'userTable', component:UserTableComponent},
  {path:'app-login', component:LoginComponent}
]
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }
