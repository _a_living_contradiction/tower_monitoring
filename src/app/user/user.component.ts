import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import {UserTableComponent} from '../user-table/user-table.component';
//import {ToastrService} from 'ngx-toastr';
import {UserFormComponent} from '../user-form/user-form.component';
import {UserModel} from '../model/user.model';
import {RegisterUserService} from '../services/register-user.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
createuser:boolean=false;
showTable:boolean=true;
user:UserModel=new UserModel();
  
constructor(private _router: Router, public RUSvc:RegisterUserService ) { }
  
  
  onCreate(){
    this.createuser=!this.createuser;
    this.showTable=!this.showTable
 }

 userChanged(event){
  this.showTable=true;
  this.createuser=false;
}

ngOnInit() {
}

  // shoeForEdit(){
  // //   this.userService.selectedUser=object.assign({},usr);;

  // this.RUSvc.UpdateUser(this.user).then(res=>{
  //   if(res.status==true){
  //     this.RUSvc.getUserDetails().then(data=>{
  //       this.user=data.data;
  //   alert("Profile Updated Successfully")
  //     })
  //   }
  //   else
  //   res.data.forEach(response => {
  //   alert(response)
  //   });
  // })
  //  }

  // onDelete(){
  //   if (confirm('Are you sure to delete this record?')==true){
  //     this.RUSvc.deleteUser();
  //     .subscribe(x => {
  //       this.RUSvc.getUserTypeList();
  //       this("Deleted Successfully!","User Register");
        
  //     })
  //   }
   
  // }

}
